package com.example.egork.calltool;


public class Dogs {

    String name;
    int sila;
    int ves;

    public static class Terier extends Dogs {
        Terier(int ves, int sila, String name) {
            this.ves = ves;
            this.sila = sila;
            this.name = name;

        }
    }
    public static class Boycovka extends Dogs {
        Boycovka(int ves, int sila, String name){
            this.ves = ves;
            this.sila = sila;
            this.name = name;
        }
    }

    public static String Fight(Dogs dog1,Dogs dog2) {
        if(dog1.sila > dog2.sila) {
            return dog1.name;
        }
        else if(dog2.sila > dog1.sila) {
            return dog2.name;
        }
        else if(dog1.sila == dog2.sila) {
            return "Ничья";
        }
        else return "Null";
    }
}
