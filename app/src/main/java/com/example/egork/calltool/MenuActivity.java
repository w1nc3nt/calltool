package com.example.egork.calltool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MenuActivity extends AppCompatActivity {
    @BindView(R.id.textView912) TextView usr;
    String username;
    Boolean isAdmin = false;
    DbActions db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        usr.setText("Welcome, " + intent.getStringExtra("User") + "!");
        username = intent.getStringExtra("User");
        if(intent.getStringExtra("Permissions").equals("admin")){isAdmin = true;}
        db = new DbActions(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu0){
        getMenuInflater().inflate(R.menu.menu_settings, menu0);
        return super.onCreateOptionsMenu(menu0);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_changepsw:
                Intent inten = new Intent(this,SettingsActivity.class);
                inten.putExtra("User",username);
                startActivityForResult(inten,1);
                break;
            case R.id.menu_exit:
                finish();
                break;
            case R.id.menu_users:
                Intent intent = new Intent(this, UsersActivity.class);
                intent.putExtra("User",username);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            finish();
        }
    }
    public void onClick(View view) {
        Intent intent;
        switch(view.getId()){
            case R.id.button5:
               if(isAdmin){
                intent = new Intent(this, MainActivity.class);
                startActivity(intent); }
                else {
                   Toast.makeText(this, "Blocked.", Toast.LENGTH_SHORT).show();
               }
                break;
            case R.id.button6:
                intent = new Intent(this, ColorActivity.class);
                startActivity(intent);
                break;
            case R.id.button75:
                if(isAdmin){
                    intent = new Intent(this, AnimActivity.class);
                    startActivity(intent); }
                else {
                    Toast.makeText(this, "Blocked.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button9:
                if(isAdmin){
                    intent = new Intent(this, DogsActivity.class);
                    startActivity(intent); }
                else {
                    Toast.makeText(this, "Blocked.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button111:
                intent = new Intent(this, IntentActivity.class);
                startActivity(intent);
                break;
        }

    }

}
