package com.example.egork.calltool;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper  extends SQLiteOpenHelper{


    public static final String TABLE_NAME = "users_table";

    public static final String KEY_ID = "_id";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_PERMISSIONS = "permissions";

    private static final String DATABASE_NAME = "CallDB";
    private static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME +" ("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_LOGIN + " text,"
                + KEY_PERMISSIONS + " text,"
                + KEY_PASSWORD + " text" + ");");

        ContentValues cv = new ContentValues();


        cv.put(KEY_LOGIN, "Root");
        cv.put(KEY_PASSWORD, "toor");
        cv.put(KEY_PERMISSIONS,"admin");
        db.insert(TABLE_NAME, null, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }
}
