package com.example.egork.calltool;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.view.View.OnClickListener;


public class ColorActivity extends AppCompatActivity implements OnClickListener {

    TextView test1;
    TextView test2;
    Button but1;
    Button but2;
    Button but3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);
        test1 = (TextView) findViewById(R.id.textViTEST1);
        test2 = (TextView) findViewById(R.id.textVITEST2);
        but1 = (Button) findViewById(R.id.but11);
        but2 = (Button) findViewById(R.id.but12);
        but3 = (Button) findViewById(R.id.button33);
        but1.setOnClickListener(this);
        but2.setOnClickListener(this);
        but3.setOnClickListener(this);
    }
    @Override
    public void onClick(View vi) {
        switch(vi.getId()){
            case R.id.but11:
                test1.setTextColor(Color.GREEN);
                break;
            case R.id.but12:
                test2.setTextColor(Color.RED);
                break;
            case R.id.button33:
                test1.setTextColor(Color.GRAY);
                test2.setTextColor(Color.GRAY);
                break;
        }
    }


}
