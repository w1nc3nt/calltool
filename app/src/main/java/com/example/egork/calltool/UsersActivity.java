package com.example.egork.calltool;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;


public class UsersActivity extends AppCompatActivity  {

    DbActions db;
    LinearLayout llay;
    LayoutInflater layInfl;
    AccountsDataModel loggedUser;
    int color = Color.RED;
    int color1 = Color.GRAY;
    Boolean isAdmin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        db = new DbActions(this);
        Intent intent = getIntent();
        loggedUser = db.getAccount(intent.getStringExtra("User"));
        if(loggedUser.getPermissions().equals("admin")){isAdmin = true;}
        getAccList();

    }

    public void getAccList(){ // Получаем список всех аккаунтов из БД
        llay = (LinearLayout) findViewById(R.id.linLayout);
        layInfl = getLayoutInflater();
        Boolean isadm;
        List<AccountsDataModel> AcmMD = db.getAccounts();
        for (AccountsDataModel Acm : AcmMD) {
            if(Acm.getPermissions().equals("admin")){ // Проверка на администратора
                isadm = true; Log.d("DB_LOG","isTrue");}
            else {
                isadm = false; Log.d("DB_LOG","isFalse");}
            addViews(Acm.getLogin(),Acm.getPassword(),Acm.getPermissions(),Acm.getId(),isadm);
        }
        db.close();
    }


    public void addViews(String login, String password, String permissions, int id, Boolean isadm){ //Создаём обьекты аккаунтов по шаблону list_item.xml
        View item = layInfl.inflate(R.layout.list_item, llay,false);
        TextView itemLogin = (TextView) item.findViewById(R.id.itemLogin);  itemLogin.setText(login);
        TextView itemPassword = (TextView) item.findViewById(R.id.itemPassword);
        TextView itemPermissions = (TextView) item.findViewById(R.id.itemPermissions); itemPermissions.setText("Group: " + permissions);
        if(isAdmin){
            itemPassword.setText("Password: " + password);}
        else {
            itemPassword.setText("Password: ****");}
        TextView itemID = (TextView) item.findViewById(R.id.itemID); itemID.setText("ID: " + Integer.toString(id));
        item.getLayoutParams().width = LayoutParams.MATCH_PARENT;
        if(isadm){item.setBackgroundColor(color);}
        else {item.setBackgroundColor(color1);}
        llay.addView(item);
        item.setId(id);
        registerForContextMenu(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        menu.add(1,view.getId(),0,"Delete");
        menu.add(2,view.getId(),0,"Set admin");
        menu.add(3,view.getId(),0,"Set user");
        super.onCreateContextMenu(menu,view,menuInfo);
    }

    public void setPerm(MenuItem item, String permissions){ // для установки пермишенов
        db.changePermissions(Integer.toString(item.getItemId()),permissions);
        db.close();
        llay.removeAllViews();
        getAccList();
    }

    public boolean onContextItemSelected(MenuItem item) {
        if(isAdmin){
            switch (item.getGroupId()) {
                case 1:
                    db.deleteAccount(null, item.getItemId());
                    db.close();
                    llay.removeAllViews();
                    Toast.makeText(this, "Account was deleted", Toast.LENGTH_SHORT).show();
                    getAccList();
                    break;
                case 2:
                    setPerm(item,"admin"); // дать админку
                    break;
                case 3:
                    setPerm(item,"user");
                    break;
            }
        }
        else {Toast.makeText(this,"You don't have permissions!",Toast.LENGTH_SHORT).show();}
        return super.onContextItemSelected(item);
    }


}
