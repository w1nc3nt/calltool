package com.example.egork.calltool;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.content.Intent;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editText) EditText login;
    @BindView(R.id.editText2) EditText password;
    DbActions db;
    @BindView(R.id.button) Button logn;
    @BindView(R.id.buttonRegistr) Button registr;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = new DbActions(this);
        ButterKnife.bind(this);
    }
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                new AsyncLogin().execute(login.getText().toString());
                break;
            case R.id.buttonRegistr:
                startActivityForResult(new Intent(this, RegisterActivity.class),1);
                break;
        }
    }
    class AsyncLogin extends AsyncTask<String,Void,AccountsDataModel> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected AccountsDataModel doInBackground(String... text){
            AccountsDataModel acm = db.getAccount(text[0]);
            db.close();
            return acm;
        }
        @Override
        protected void onPostExecute(AccountsDataModel result){
            super.onPostExecute(result);
            Authentific(result.getLogin(),result.getPassword(),result.getPermissions());
            progressBar.setVisibility(View.INVISIBLE);
        }
    }
    public void Authentific(String dblogin,String dbpassword, String dbpermissions){
        if(login.getText().toString().equals(dblogin) && password.getText().toString().equals(dbpassword)){
            Intent success = new Intent(this,MenuActivity.class);
            success.putExtra("User",login.getText().toString());
            success.putExtra("Permissions",dbpermissions);
            startActivity(success);
            password.setText("");
        }
        else {Toast.makeText(this,"Login or password incorrect",Toast.LENGTH_SHORT).show();}
    }
}
