package com.example.egork.calltool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class AnimActivity extends AppCompatActivity {


    Button bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim);
        bt = (Button) findViewById(R.id.tv);
        registerForContextMenu(bt);

    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        switch (v.getId()) {
            case R.id.tv:
                menu.add(0, 1, 0, "alpha");
                menu.add(0, 2, 0, "scale");
                menu.add(0, 4, 0, "test");
                break;
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public boolean onContextItemSelected(MenuItem item) {
        Animation anim = null;

        switch (item.getItemId()) {
            case 1:
                anim = AnimationUtils.loadAnimation(this, R.anim.myalpha);
                break;
            case 4:
                anim = AnimationUtils.loadAnimation(this, R.anim.myrotate);
                break;
            case 2:
                anim = AnimationUtils.loadAnimation(this, R.anim.mytest);
                break;
        }
        bt.startAnimation(anim);
        return super.onContextItemSelected(item);
    }
}
