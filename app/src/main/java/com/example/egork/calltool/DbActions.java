package com.example.egork.calltool;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import com.example.egork.calltool.AccountsDataModel;

/**
 * Created by egork on 18.10.2017.
 */

public class DbActions {
    DbHelper dbHelper;
    Context context;
    Cursor cursor;
    SQLiteDatabase db;
    List<AccountsDataModel> mAccountsList;

    public DbActions(Context context) {
        this.context = context;
        dbHelper = new DbHelper(context);
    }
    // возвращает количество записей в таблице
    public int getItemCount() {

        db = dbHelper.getReadableDatabase();

        cursor = db.query(DbHelper.TABLE_NAME, null, null, null, null, null, null);
        int cnt = cursor.getCount();
        cursor.close();

        return cnt;
    }
    // метод для cмены password'а
    public void changePassword(String login, String newPassword){
        db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.KEY_PASSWORD, newPassword);
        String[] args = new String[]{login};
        db.update(DbHelper.TABLE_NAME, cv, "login = ?", args);
    }

    public void changePermissions(String id, String permissions){ // Установка прав юзера(admin/user)
        db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.KEY_PERMISSIONS,permissions);
        String[] args = new String[]{id};
        db.update(DbHelper.TABLE_NAME, cv, "_id = ?", args);
    }

    public void addAccount(String login, String password, String adminPermiss){
        db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.KEY_LOGIN, login);
        cv.put(DbHelper.KEY_PASSWORD, password);
        if(adminPermiss == "admin"){cv.put(DbHelper.KEY_PERMISSIONS,adminPermiss);} else {cv.put(DbHelper.KEY_PERMISSIONS,"user");}
        db.insert(DbHelper.TABLE_NAME,null,cv);

    }

    public AccountsDataModel getAccount(String login){
        db = dbHelper.getReadableDatabase();
        String[] loginArgs = new String[]{login};
        cursor = db.query(DbHelper.TABLE_NAME,null,"login = ?",loginArgs,null,null,null);
        AccountsDataModel acmd = new AccountsDataModel(null,null,null,null);
        if(cursor.moveToFirst()){acmd = new AccountsDataModel(cursor.getInt(cursor.getColumnIndex(DbHelper.KEY_ID)), cursor.getString(cursor.getColumnIndex(DbHelper.KEY_LOGIN)),
                cursor.getString(cursor.getColumnIndex(DbHelper.KEY_PASSWORD)), cursor.getString(cursor.getColumnIndex(DbHelper.KEY_PERMISSIONS))); cursor.close();}
        else {}
        return acmd;

    }
    // метод для удаления строки по login
    public void deleteAccount(String login, int id) {
        db = dbHelper.getWritableDatabase();
        if(login != null) {
            db.delete(DbHelper.TABLE_NAME, DbHelper.KEY_LOGIN + "=" + login, null);
        }
        else {
            db.delete(DbHelper.TABLE_NAME,DbHelper.KEY_ID + "=" + id,null);
        }
    }
    // метод возвращающий коллекцию всех данных
    public List<AccountsDataModel> getAccounts() {
        db = dbHelper.getReadableDatabase();
        cursor = db.query(DbHelper.TABLE_NAME, null, null, null, null, null, null);
        mAccountsList = new ArrayList<AccountsDataModel>();

        if (cursor.moveToFirst()) {

            int idColInd = cursor.getColumnIndex(DbHelper.KEY_ID);
            int loginColInd = cursor.getColumnIndex(DbHelper.KEY_LOGIN);
            int passwordColInd = cursor.getColumnIndex(DbHelper.KEY_PASSWORD);
            int permissionsColInd = cursor.getColumnIndex(DbHelper.KEY_PERMISSIONS);
            do {
                AccountsDataModel AccDM = new AccountsDataModel(cursor.getInt(idColInd),
                        cursor.getString(loginColInd), cursor.getString(passwordColInd), cursor.getString(permissionsColInd));
                mAccountsList.add(AccDM);
            } while (cursor.moveToNext());

        } else {
        }

        cursor.close();

        return mAccountsList;

    }
    // здесь закрываем все соединения с базой и класс-помощник
    public void close() {
        dbHelper.close();
        db.close();
    }
}
