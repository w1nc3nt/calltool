package com.example.egork.calltool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.net.Uri;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class WebBrowser extends AppCompatActivity {

    EditText gotext;
    WebView wbview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);
        wbview = (WebView) findViewById(R.id.webVi);
        wbview.setWebViewClient(new WebViewClient());
        wbview.loadUrl(String.valueOf(Uri.parse(getIntent().getData().toString())));
        gotext = (EditText) findViewById(R.id.editURL);
    }

    public void onClick(View vi) {
        wbview.loadUrl(String.valueOf(Uri.parse("http://"+gotext.getText().toString())));
    }
}
