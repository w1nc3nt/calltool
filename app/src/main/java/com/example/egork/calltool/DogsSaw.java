package com.example.egork.calltool;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;

public class DogsSaw extends AppCompatActivity implements OnClickListener {

    Button start;
    EditText name;
    EditText ves;
    EditText sila;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dogssaw);
        start = (Button) findViewById(R.id.buttonOkay);
        start.setOnClickListener(this);
        name = (EditText) findViewById(R.id.editName);
        ves = (EditText) findViewById(R.id.editVes);
        sila = (EditText) findViewById(R.id.editSila);

    }

    public void onClick(View vi) {
        Intent intent = new Intent();
        intent.putExtra("Name", name.getText().toString());
        intent.putExtra("Ves", Integer.parseInt(ves.getText().toString()));
        intent.putExtra("Sila", Integer.parseInt(sila.getText().toString()));
        setResult(RESULT_OK,intent);
        finish();
    }
}
