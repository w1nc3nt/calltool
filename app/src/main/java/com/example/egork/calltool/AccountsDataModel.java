package com.example.egork.calltool;

public class AccountsDataModel {
    private Integer id;
    private String login;
    private String password;
    private String permissions;

    AccountsDataModel(){}

    public AccountsDataModel(Integer id, String login, String password, String permissions ) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.permissions = permissions;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPermissions(){ return permissions; }
    public void setPermissions(String permissions){ this.permissions = permissions; }
}
