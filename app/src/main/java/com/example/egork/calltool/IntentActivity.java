package com.example.egork.calltool;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class IntentActivity extends AppCompatActivity implements OnClickListener {

    Button bt1;
    Button bt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        bt1 = (Button) findViewById(R.id.buttonCall);
        bt1.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.button432);
        bt2.setOnClickListener(this);
    }
    @Override
    public void onClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.buttonCall:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:466"));
                startActivity(intent);
                break;
            case R.id.button432:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://startandroid.ru/ru/uroki"));
                startActivity(intent);
                break;
        }
    }
}
