package com.example.egork.calltool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    DbActions db;
    EditText nlogin;
    EditText npassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        db = new DbActions(this);
        nlogin = (EditText) findViewById(R.id.editTextNL);
        npassword = (EditText) findViewById(R.id.editTextNP);
    }

    public void onClick(View vi){
        Intent intent = new Intent();
        db.addAccount(nlogin.getText().toString(),npassword.getText().toString(),"user");
        db.close();
        setResult(RESULT_OK,intent);
        Toast.makeText(this,"Account was created!",Toast.LENGTH_SHORT).show();
        finish();
    }
}
