package com.example.egork.calltool;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import com.example.egork.calltool.Dogs.*;
import android.view.View.OnClickListener;
import android.widget.Toast;


public class DogsActivity extends AppCompatActivity implements OnClickListener {

    Button start;
    TextView result;
    Button createDog;
    Button resetButton;
    int whichOfDogs = 0;
    Dogs Yegor;
    Dogs Valera;
    TextView REDDOG;
    TextView GREENDOG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dogs);
        result = (TextView) findViewById(R.id.textResultat);
        start = (Button) findViewById(R.id.buttonStart);
        start.setOnClickListener(this);
        createDog = (Button) findViewById(R.id.buttonCreateDog);
        createDog.setOnClickListener(this);
        start.setVisibility(View.INVISIBLE);
        resetButton = (Button) findViewById(R.id.buttonReset);
        resetButton.setOnClickListener(this);
        REDDOG = (TextView) findViewById(R.id.textRedDog);
        GREENDOG = (TextView) findViewById(R.id.textGreenDog);
    }
    @Override
    public void onClick(View vi){
        switch (vi.getId()){
            case R.id.buttonCreateDog:
                Intent intent = new Intent(this, DogsSaw.class);
                startActivityForResult(intent, 1);
                whichOfDogs++;
                break;
            case R.id.buttonStart:
                result.setTextSize(15);
                String winner = Dogs.Fight(Yegor,Valera);
                if(winner.equals(Yegor.name)) {result.setTextColor(Color.RED);}
                else {result.setTextColor(Color.GREEN);}
                result.setText(winner + " is win!");
                break;
            case R.id.buttonReset:
                result.setText("Create first dog.");
                result.setTextColor(Color.BLACK);
                createDog.setVisibility(View.VISIBLE);
                start.setVisibility(View.INVISIBLE);
                GREENDOG.setText(""); REDDOG.setText("");
                whichOfDogs = 0;
                break;

        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) {return;}
        if(whichOfDogs == 1){
        Yegor = new Boycovka(data.getIntExtra("Ves",1),data.getIntExtra("Sila",1),data.getStringExtra("Name")); result.setTextSize(15); result.setText("Create second dog.");
         REDDOG.setText(data.getStringExtra("Name")); Toast.makeText(this, data.getStringExtra("Name") + "was created." , Toast.LENGTH_SHORT).show();}
        if(whichOfDogs == 2){
        Valera = new Terier(data.getIntExtra("Ves",1),data.getIntExtra("Sila",1),data.getStringExtra("Name")); createDog.setVisibility(View.INVISIBLE); start.setVisibility(View.VISIBLE);
          result.setText("Press Start!");
          GREENDOG.setText(data.getStringExtra("Name")); Toast.makeText(this, data.getStringExtra("Name") + " was created." , Toast.LENGTH_LONG).show();
        }

    }
}

