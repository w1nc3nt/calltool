package com.example.egork.calltool;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    DbActions db;
    EditText newpass;
    EditText oldpass;
    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        db = new DbActions(this);
        newpass = (EditText) findViewById(R.id.editNewPass);
        oldpass = (EditText) findViewById(R.id.editOldPass);
    }

    public Boolean checkEqualing(String oldp){
        Boolean status =false;
        Intent intent = getIntent();
        Log.d("DB_LOG",intent.getStringExtra("User"));
        List<AccountsDataModel> AcmD = db.getAccounts();
        for(AccountsDataModel Acm : AcmD){
            if(intent.getStringExtra("User").equals(Acm.getLogin())){
                user = intent.getStringExtra("User");
                if(Acm.getPassword().equals(oldp)){status = true; break;}
            }
            else{status = false;}
        }
        return status;
    }



    public void onClick(View view){
        if(checkEqualing(oldpass.getText().toString())){
            db.changePassword(user,newpass.getText().toString());
            db.close();
            Toast.makeText(this,user + ", your password was changed!",Toast.LENGTH_SHORT).show();
            Intent inta = new Intent();
            setResult(RESULT_OK,inta);
            finish();
        }
        else{Toast.makeText(this,"Old password is wrong.",Toast.LENGTH_SHORT).show(); oldpass.setText("");}
    }
}
